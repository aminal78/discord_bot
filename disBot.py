import discord

# ساخت یک کلاینت دیسکورد
client = discord.Client()

# هنگامی که ربات به دیسکورد متصل می‌شود، این تابع اجرا می‌شود
@client.event
async def on_ready():
    print(f'We have logged in as {client.user}')

# هنگامی که پیامی در چت ارسال می‌شود، این تابع اجرا می‌شود
@client.event
async def on_message(message):
    if message.author == client.user:
        return

    if message.content.startswith('$hello'):
        await message.channel.send('Hello!')

# وارد کردن توکن ربات
client.run('a953c7d5358bd3750a4e452fd9f33e756415a0daa9699091a7c3884e23d1158b')


# app_id = 1253351319209508966